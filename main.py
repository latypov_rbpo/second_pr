import os.path
from threading import Thread
from multiprocessing import Lock, Process, Queue, current_process, Pool
import queue
import pickle
import time
start_time = time.time()
#Decryptor.bulkhead('1115dd800feaacefdf481f1f9070374a2a81e27880f187396db67958b207cbad')
#Decryptor.bulkhead('3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b')
#Decryptor.bulkhead('74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f')


def check(dict, hash, th):
    for key in dict:
        if dict[key] == hash:
            print(key)
            return


def split_dict(input_dict: dict, num_parts: int) -> list:
    list_len: int = len(input_dict)
    return [dict(list(input_dict.items())[i * list_len // num_parts:(i + 1) * list_len // num_parts])
        for i in range(num_parts)]

"""def multiProcessSearch(ProcessCount, hash):
    with open("mySavedDict.txt", "rb") as myFile:
        passDictionary = pickle.load(myFile)
    print("worker" + "{number}".format(number=ProcessCount))
    time.sleep(1)
    return
    starttime = time.time()
    GROUP_START = 1
    GROUP_END = 20
    if __name__ == '__main__':
        pool = Pool(4)
        group = ([x for x in range(GROUP_START,
                                   GROUP_END + 1)])  # Группа в процессе в основном не используется, но это не процесс, но это не
        pool.map(worker, group)
        pool.close()
        pool.join()
    endtime = time.time()
    dtime = endtime - starttime
    print("Время выполнения программы:% .8s s" % dtime)"""


def multithreadedSearch(threadCount, hash):
    with open("mySavedDict.txt", "rb") as myFile:
        passDictionary = pickle.load(myFile)
    splittedDict = split_dict(passDictionary, threadCount)
    th = []
    for i in range(0, threadCount):
        th.append(Thread(target=check, args=(splittedDict[i], hash, th, )))
        th[i].start()
#multithreadedSearch(1, '1115dd800feaacefdf481f1f9070374a2a81e27880f187396db67958b207cbad')
#multithreadedSearch(1, '3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b')
multithreadedSearch(5, '74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f')
#multiProcessSearch(2, '74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f')
print(time.time() - start_time)